window.onload = function(){
	var number; //Введенное число
    var originalScaleOfNotation; //Введенная СС числа
    var resultScaleOfNotation; //СС в которую нужно перевести
   
    //Получение числа и исходной СС
    function InputNumber(){
        window.number = document.getElementById("OrigNum").value;
        window.originalScaleOfNotation = document.getElementById("ScaleOfNotationFrom").value;
        window.resultScaleOfNotation = document.getElementById("ScaleOfNotationTo").value;
        if(window.originalScaleOfNotation == 16){
            window.number= window.number.toUpperCase();
        }
        return 0;
    }

    function TranslationOfNumber(){
    	InputNumber();
    	var checkError = validation(window.number, window.originalScaleOfNotation, window.resultScaleOfNotation);
    	var newNumber = parseInt(window.number, window.originalScaleOfNotation);
    	var resultOfTranslation;

    	//Проверка корректности ввода
        if(checkError != -1){
            //Выполнение перевода
            document.getElementById("result").innerHTML="Результат перевода:";
            resultOfTranslation=newNumber.toString(window.resultScaleOfNotation);
            OutputResult(window.resultScaleOfNotation, resultOfTranslation);
        }
    }
    //Формат вывода
    function Output(str, resultScaleOfNotation, resultOfTranslation){
        if(resultScaleOfNotation == 16){
            document.getElementById(str).innerHTML=window.number+'<sub>'+window.originalScaleOfNotation+'</sub>'+
            "="+resultOfTranslation.toUpperCase()+'<sub>'+resultScaleOfNotation+'</sub>';
        }
        else{
            document.getElementById(str).innerHTML=window.number+'<sub>'+window.originalScaleOfNotation+'</sub>'+"="+
            resultOfTranslation+'<sub>'+resultScaleOfNotation+'</sub>';
        }
        return 0;
    }

    //Вывод результата перевода
    function OutputResult(resultScaleOfNotation, resultOfTranslation){
        Output("str", window.resultScaleOfNotation, resultOfTranslation);
        return 0;
    }

	//Проверка корректности ввода
    function validation(number, originalScaleOfNotation, resultScaleOfNotation){
        //Если не введено число
        if(	number==false){
            alert("Введите число.");
            return -1;
        }
        
        switch(originalScaleOfNotation){
            case "2":
                if(/[А-Яа-яA-Za-z_]/.test(number) || /[,.:;!?@"#№$%^&\*\-+=()_\|<>]/.test(number) || /[2-9]/.test(number)){
                    alert("Для ввода двоичного числа используются цифры 0 и 1");
                    return -1;
                }
                break;
            case "8":
                if(/[А-Яа-яA-Za-z_]/.test(number) || /[,.:;!?@"#№$%^&\*\-+=()_\|<>]/.test(number) || /[8-9]/.test(number)){
                    alert("Для ввода восьмеричного числа используются цифры 0 - 7");
                    return -1;
                }
                break;
            case "10":
                if(/[А-Яа-яA-Za-z_]/.test(number) || /[,.:;!?@"#№$%^&\*\-+=()_\|<>]/.test(number)){
                    alert("Для ввода десятичного числа используются цифры 0 - 9");
                    return -1;
                }
                break;
            case "16":
                if(/[А-Яа-яG-Zg-z_]/.test(number) || /[,.:;!?@"#№$%^&\*\-+=()_\|<>]/.test(number)){
                    alert(" Для ввода шестнадцатиричного числа используются цифры 0-9, буквы A,B,C,D,E,F");
                    return -1;
                }
                break;
        }
    }   
    document.getElementById("translation").onclick = TranslationOfNumber;
}

