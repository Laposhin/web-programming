window.onload = function(){
    
    function validate(){
        //Проверка фамилии
        validate_output(/^[a-zA-Zа-яА-Я0-9-_\.]{1,}$/,
                        document.getElementById("SurName").value, "SurNameSpan", "Введите фамилию! Не менее 1 символа");
        //Проверка имени
		validate_output(/^[a-zA-Zа-яА-Я0-9-_\.]{1,}$/,
                        document.getElementById("NameUser").value, "UNameSpan", "Введите имя! Не менее 1 символа");
		//Проверка даты
		validate_output(/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/,
                        document.getElementById("DateUser").value, "UDateSpan", "Введите дату рождения");
        //Проверка телефона
		validate_output(/^((8|\+7)(\-| |\()?)?(\d){2,3}(\-| |\))?(\d){2,3}(\-| )?((\d){2}(\-| )?)?((\d){2}(\-| )?)?$/, 
                        document.getElementById("phone").value, "PhoneSpan", "Введите телефон! Не менее 6 символов");
        //Проверка e-mail
		validate_output(/^[a-zA-Zа-яА-Я0-9-_\.]+@[a-zA-Zа-яА-Я0-9-_\.]+\.[A-z]{2,4}$/,
                        document.getElementById("UserEmail").value, "emailSpan", "Введите email! Email содержит @");
        //Проверка пароля
       /* validate_output(/^[A-zА-я0-9$/,
                        document.getElementById("Password").value, "PasswordSpan", "Пароль должен состоять из лат. букв,цифр,"_","-" и "." и быть не меньше 8 символов! ");*/
		
    }
   
    function validate_output(reg, str, id, message){
        if(!reg.test(str)){
			document.getElementById(id).innerHTML = message;
        }
		else{
			document.getElementById(id).innerHTML = "";  
		}
    }

    document.getElementById("button_id").onclick = validate;
}